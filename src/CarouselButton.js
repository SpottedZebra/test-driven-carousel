import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DefaultButton = styled.button`
  background-color: purple;
  color: white;
  height: 50px;
  width: 50px;
  padding: 10px;
  text-decoration: underline;
  font-weight: 900;
`;

const CarouselButton = props => <DefaultButton {...props} />;

CarouselButton.defaultProps = {
  DefaultButton: DefaultButton,
};

CarouselButton.propTypes = {
  DefaultButton: PropTypes.elementType,
};

export default CarouselButton;
